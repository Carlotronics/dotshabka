# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aminueza/minio" {
  version     = "1.2.0"
  constraints = "1.2.0"
  hashes = [
    "h1:nK2UMZIzAEleLzRp3CbtIjPWT8FLrrGElwfH01E5Q7k=",
    "zh:4a6d917ab8107d1f63427bf424d06e694572a67d77dbc8e10500ddcbf792088c",
    "zh:5b1239c1286367bc09c9804795c0fd7d8defd190d8a3b2822b5db855b451529f",
    "zh:5c28ed37ae527486113b4e380408d5214235f44fed1ea46d96d57caae664f6ba",
    "zh:83ebb631f40648eb600ebbd72a8a9ee70243031a816e255dd3ad110e5c655025",
    "zh:9bbd045d974b64e974a8d48467e84370246135ad13f04cc5e5473f8c78e9013a",
    "zh:a55af43fd7109e24de108e5b955a63f09c33ff8374c1452edeee929a55ac23ee",
    "zh:af4434939ecf5965da71eebc46da5f069e4a9c501bbf81568fb9a2ef9017dd45",
    "zh:bf534d37b1b020115982a284b86f6a3abee48d64aa4d57c2055c58b8566c21f2",
    "zh:d70ce773c632a2710290ce029f233411a5a7146d3d658d8bd9a6d7b27a3eb6fb",
    "zh:e267702a8a68269419abc53065f90726dcb61628cecba66d1b24f0a6e062a44c",
    "zh:f98e077099c4b6c178deff11c8d60ca0031cdf8a05bbbaa1cc444135c00a9638",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.1.0"
  constraints = "3.1.0"
  hashes = [
    "h1:BZMEPucF+pbu9gsPk0G0BHx7YP04+tKdq2MrRDF1EDM=",
    "zh:2bbb3339f0643b5daa07480ef4397bd23a79963cc364cdfbb4e86354cb7725bc",
    "zh:3cd456047805bf639fbf2c761b1848880ea703a054f76db51852008b11008626",
    "zh:4f251b0eda5bb5e3dc26ea4400dba200018213654b69b4a5f96abee815b4f5ff",
    "zh:7011332745ea061e517fe1319bd6c75054a314155cb2c1199a5b01fe1889a7e2",
    "zh:738ed82858317ccc246691c8b85995bc125ac3b4143043219bd0437adc56c992",
    "zh:7dbe52fac7bb21227acd7529b487511c91f4107db9cc4414f50d04ffc3cab427",
    "zh:a3a9251fb15f93e4cfc1789800fc2d7414bbc18944ad4c5c98f466e6477c42bc",
    "zh:a543ec1a3a8c20635cf374110bd2f87c07374cf2c50617eee2c669b3ceeeaa9f",
    "zh:d9ab41d556a48bd7059f0810cf020500635bfc696c9fc3adab5ea8915c1d886b",
    "zh:d9e13427a7d011dbd654e591b0337e6074eef8c3b9bb11b2e39eaaf257044fd7",
    "zh:f7605bd1437752114baf601bdf6931debe6dc6bfe3006eb7e9bb9080931dca8a",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.0.1"
  constraints = "3.0.1"
  hashes = [
    "h1:07U4BKuNGdwbBDpY7stiDu4Foh/PoiJV+U9Je49MQd8=",
    "h1:0E6lQGHly25Ywk1psqd+bEL0a2TSzvvqOIuROLo3WyI=",
    "h1:9S1SB7t2nEby7TYAA5M2O7jbvgmFIvWksiXzoAu1ZUM=",
    "h1:TOBDaHch1mKRqFRBGM/UUtVrjw1UOqNLGP9n+Db/yFs=",
    "h1:Vo3ItGUSW4Yw9vofboA8tCZRlMvmlE+agIPgfLZbwzQ=",
    "h1:aP2sQh2cR7ifV5WpQzUxZORLWxqnOlCHtIFkRrSXBsg=",
    "h1:fAGlovU4YJleg0//hNHVPXHO8cjZ3/qDcnqOywvcG1Y=",
    "h1:gWD+O2MRKapQD9xgQA0+HEpHB2qXQpgQrWdaQMK+qm0=",
    "h1:kIxg40hoCkXxfJkvJLrhiznQmcUNQP+kIR1hbhV5byM=",
    "h1:o4EdexC2niJZsnJzvP2AfReA788OGMkE5ijPEc1q4m4=",
    "h1:rSTd1HkPW9YB/U8PZZX4ueHGdWPdKnb9PfvxXU/z8E0=",
  ]
}
